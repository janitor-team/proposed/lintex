lintex (1.14-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Set Vcs-* to salsa.debian.org

  [ Helmut Grohne ]
  * Fix FTCBFS, 03_crossbuild.patch (Closes: #929240)
  * Replace stripping make install with debian/lintex.install

  [ Ryan Kavanagh ]
  * Bump standards-version to 4.4.0
  * Migrate copyright file to dep-5 format
  * Fix spelling mistake, 04_typo.diff

 -- Ryan Kavanagh <rak@debian.org>  Sat, 17 Aug 2019 11:37:50 -0400

lintex (1.14-1) unstable; urgency=medium

  * Imported Upstream version 1.14
    + Fix incorrect keys in manpage (Closes: #693595)
    + Refresh patches
  * Bump standards version to 3.9.6
  * Update copyright file
  * Update watch file to no longer use githubredir
  * Make Vcs-* fields canonical

 -- Ryan Kavanagh <rak@debian.org>  Sun, 08 Feb 2015 18:59:06 -0500

lintex (1.13-1) unstable; urgency=low

  * Imported Upstream version 1.13
  * Added .pc to .gitignore
  * Switch to @debian.org and drop DMUA
  * Bump standards to 3.9.3
  * Bump debhelper to >= 9 and compat to 9
  * Build-depend on libconfig-dev and pkg-config
  * Don't force the use of gcc, 03_dont_force_gcc.diff
  * Fixed typo in manpage, 04_man_typo.diff
  * Use dh's CFLAGS/CPPFLAGS/LDFLAGS, 05_CFLAGS.diff
  * Install lintexrc as an example
  * Fix watchfile to properly name tarball

 -- Ryan Kavanagh <rak@debian.org>  Mon, 10 Sep 2012 19:14:35 +0100

lintex (1.11-1) unstable; urgency=low

  * Imported Upstream version 1.11
    - Removes .synctex.gz files (Closes: #647691)
  * Bump standards version to 3.9.2

 -- Ryan Kavanagh <ryanakca@kubuntu.org>  Mon, 07 Nov 2011 13:40:55 -0500

lintex (1.10-1) unstable; urgency=low

  * Imported Upstream version 1.10
    - Adds list of removal extensions to manpage and removes .thm files,
      (Closes: #611523)
  * Updated copyright holders' years

 -- Ryan Kavanagh <ryanakca@kubuntu.org>  Sun, 30 Jan 2011 14:11:08 -0500

lintex (1.09-1) unstable; urgency=low

  * Imported Upstream version 1.09
    - Adds support for removing files older than their source, (Closes:
      #604989, #604985)
  * Added git-buildpackage config file (debian/gbp.conf)
  * Unapply patches after build to keep git quiet
  * Added DEP3 headers to 02_install_dir.diff
  * Bump debhelper to version 8
  * Bump compat accordingly

 -- Ryan Kavanagh <ryanakca@kubuntu.org>  Tue, 30 Nov 2010 19:51:05 -0500

lintex (1.08-1) unstable; urgency=low

  * New upstream release
    - Has various verbosity levels, defaults to only printing the files that
      are removed instead of printing both those which get removed and those
      which don't (Closes: #597271)
  * Updated watch file so that uscan doesn't think there's a slash in
    front of the version
  * Turned on DM-Upload-Allowed

 -- Ryan Kavanagh <ryanakca@kubuntu.org>  Fri, 01 Oct 2010 19:40:20 -0400

lintex (1.07-1) unstable; urgency=low

  * New upstream release
    - Doesn't delete read only files (Closes: #588777)
    - Includes option to only delete intermediary files (Closes: #588779)
  * Drop 01_update_manpage.diff, no longer needed
  * Drop 03_hyphen-minus-sign.diff, applied upstream
  * Added new upstream author / download location to copyright
  * Added watch file
  * Bumped standards version to 3.9.1, no changes required
  * Updated Homepage: field
  * Added Vcs-* tags
  * Fix typo in long description

 -- Ryan Kavanagh <ryanakca@kubuntu.org>  Wed, 11 Aug 2010 19:41:24 -0400

lintex (1.06-1) unstable; urgency=low

  * Initial release (Closes: #581881)

 -- Ryan Kavanagh <ryanakca@kubuntu.org>  Mon, 21 Jun 2010 17:30:15 -0400
